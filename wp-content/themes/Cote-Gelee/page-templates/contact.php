<?php
/**
 * Template Name: Contact
 */

get_header(); ?>

<?php 


	$contact_data = get_field('contact_data', 'options');
	$phone =  $contact_data['phone'];
	
?>


<div class="main-container">
	<div class="main-grid1">
		<div class="contact-main-cont ">

			<div class="cust-page-header">
				
			
			
				<h2><?php echo the_title(); ?></h2>

				<hr id="hr1">
				<hr id="hr2">

				<!-- <div class="contact-innercont cust-page-innerheader">
					<h3>Get in Touch with Us Today and Start Your Journey</h3>
					<p>Nullam quis risus eget urna mollis ornare vel eu leo. Curabitur blandit tempus porttitor. Nullam id dolor id nibh ultricies vehicula ut id elit. Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
				</div> -->

			</div>

			

				<div class="wp-block-group">
					<div class="wp-block-group__inner-container full-width">
						<div class="q-contact-cont no-padding" style="padding-bottom: 5%;">

							<?php while ( have_posts() ) : the_post(); ?>
									<?php get_template_part( 'template-parts/content', 'page' ); ?>
									<?php comments_template(); ?>
							<?php endwhile; ?>
						</div>
					</div>
				</div>

				<hr id="hr1">
				<hr id="hr2">

				<div class="contact-map-cont">
					<div class="contact-map-innercont">
						<div class="contact-halfcont cont-left">
							<div class="contact-map-innercont">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13801.309677702739!2d-91.9920955!3d30.1420537!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xddcbae0f707c9aa2!2zVmlsbGUgZGUgQ8O0dMOpIEdlbMOpZQ!5e0!3m2!1sen!2sus!4v1586263894898!5m2!1sen!2sus" width="425" height="318.75" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
						</div>
						<div class="contact-halfcont cont-right">
							<img src="/wp-content/themes/Cote-Gelee/dist/assets/images/main-logo.png">
							<div class="contact-text-cont">
								<p id="cp1">1300 Youngsville Hwy</p>
								<p>Broussard, LA</p>
								<!-- <br> -->
								<a href="tel:<?php echo $phone; ?>"><p><?php echo $phone; ?></p></a>
							</div>
						</div>
					</div>
				</div>

		</div>	
	</div>
</div>
<?php get_footer();
