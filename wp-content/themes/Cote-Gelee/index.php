<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-container">

	<div class="main-grid1 mobile-blog-grid">

		<div class="blog-header">
			<h2>In The Headlines</h2>

			<hr id="hr1">
			<hr id="hr2">

			<div class="blog-cat-cont">
				<?php wp_dropdown_categories( 'show_option_none=Select category' ); ?>
				<p>Filter by Category</p>
			</div>

			
			<script type="text/javascript">
			    
			    var dropdown = document.getElementById("cat");
			    function onCatChange() {
			        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
			            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
			        }
			    }
			    dropdown.onchange = onCatChange;
			   
			</script>

		</div>
		
		<main class="main-content-full-width main-blog-cont">
			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
			<?php endif; // End have_posts() check. ?>
		
			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php if ( function_exists( 'foundationpress_pagination' ) ) :
					foundationpress_pagination(); ?>
				<?php elseif ( is_paged() ) : ?>
				
					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php endif; ?>
		</main>
	</div>
</div>
<?php get_footer();
