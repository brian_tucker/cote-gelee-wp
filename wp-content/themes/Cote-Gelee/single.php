<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<!-- <?php get_template_part( 'template-parts/featured-image' ); ?> -->
<div class="main-container main-single-blog-cont">

	<div class="main-grid main-single-blog-grid ">
		<h2><?php echo the_title(); ?></h2>
		<div class="single-blog-date-cont">
			<div class="left-blog-hr-cont">
				<hr class="left-blog-hr1">
				<hr class="left-blog-hr2">
			</div>

			<!-- setting the date format to m.d.y -->
			<h3><?php echo get_the_date('m.d.y'); ?></h3>

			<div class="right-blog-hr-cont">
				<hr class="right-blog-hr1">
				<hr class="right-blog-hr2">
			</div>
		</div>

		<!-- this is the content from the backend -->
		<main class="main-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</main>

		<div class="social-block-full">
			<h4>Share This Article</h4>
			<div class="social-innerblock-full">
				<div class="social-block-cont">
					<div class="social-block">
						<a class="share facebook" href=""><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/facebook-icon.png"></a>
					</div>
					<div class="vertical"></div>
					<div class="social-block">
						<a class="share twitter" href=""><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/twitter-icon.png"></a>
					</div>
				</div>
				<div>
					<div class="vertical"></div>
				</div>
				<div class="social-block-cont">
					<div class="social-block">
						<a class="share linkedin" href=""><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/linkedin-icon.png"></a>
					</div>
					<div class="vertical"></div>
					<div class="social-block">
						<a href="javascript:emailCurrentPage()"><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/email-icon.png"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="blog-related-block main-blog-cont">
			<h4>Related News</h4>

			<!-- this is getting the related posts  -->
			<?php

				$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'post__not_in' => array($post->ID) ) );
				if( $related ) foreach( $related as $post ) {
				setup_postdata($post); ?>
				<div class="inner-blog-cont">
					 <p class="inner-blog-date" ><?php echo get_the_date("m.d.y"); ?></p>
					 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="display: flex; align-items: center;">
							 <header>
							 <?php
								 if ( is_single() ) {
									 the_title( '<h1 class="entry-title">', '</h1>' );
								 } else {
									 the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
								 }
							 ?>
								 <?php foundationpress_entry_meta(); ?>
							 </header>
							 <div class="thumb-cont">
								 <?php
									 if ( has_post_thumbnail() ) {
											 the_post_thumbnail();
									 }
								 ?>
							 </div>
							 <div class="entry-content">
								 <!-- <?php the_content(); ?> -->
								 <h4><?php the_title(); ?></h4>
								 <p><?php echo get_the_excerpt(); ?></p>
								 <button><a href="<?php the_permalink(); ?>"> Read More</a></button>
								 <!-- <?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?> -->
							 </div>

							 <footer>
								 <?php
									 wp_link_pages(
										 array(
											 'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
											 'after'  => '</p></nav>',
										 )
									 );
								 ?>
								 <?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
							 </footer>
					 </article>
				</div>
				<hr class="blog-hr" style="">
				<?php }
				wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	//run these when document is started
	$( document ).ready(function() {
	    
	});

	// function that allows page to be emailed
	function emailCurrentPage(){
        window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
    }

	/* ====================
	 * Facebook Initialization
	 * ==================== */
  	window.fbAsyncInit = function() {
    	FB.init({
	      appId            : '3172223912871307',
	      autoLogAppEvents : true,
	      xfbml            : true,
	      version          : 'v3.0'
    	});
  	};

  	(function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
   	}(document, 'script', 'facebook-jssdk'));


	$('.share.facebook').click(function(event) {
		event.preventDefault();
	  	FB.ui({
	  		display: 'popup',
	    	method: 'share',
	    	href: encodeURI(window.location.href),
	  	}, function(response){});
	});

	$('.share.twitter').click(function(event) {
		event.preventDefault();
		var pageTitle = '<?php echo get_the_title(); ?>';
		var text = encodeURI(pageTitle.trim());
		var url = encodeURI(window.location.href);
		var tweetShareLink = "https://twitter.com/intent/tweet?url=" + url + "&text=" + text + "&via=xxxxxx";
	  
	  	window.open(tweetShareLink,'pagename','resizable,height=420,width=550');
	});

	$('.share.linkedin').click(function(event) {
		event.preventDefault();
		var pageTitle = '<?php echo get_the_title(); ?>';
		var text = encodeURI(pageTitle.trim());
		var url = encodeURI(window.location.href);
		var linkedinShareLink = "https://www.linkedin.com/shareArticle?url=" + url + "&mini=true&title=" + text + "&source=Name%20SecondName";
	  
	  	window.open(linkedinShareLink,'pagename','resizable,height=420,width=550');
	});

</script>
<?php get_footer();
