<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer class="footer-container">
	<div class="footer-grid">
		<?php $foot_info = get_field('footer_info', 'options'); ?>

		<!-- The ones below are on the same row -->

		<div class="foot-cont">


			<div class="foot-info-box-cont">
				<div class="foot-info-box">
					<img id="main-logo-white" src="/wp-content/themes/Cote-Gelee/dist/assets/images/main-logo-white.png">
					<div class="vertical"></div>
					<div class="foot-boxes">
						<div class="foot-box-1">
							<p>
								<?php echo $foot_info['address_line_1']?>
							</p>
							<p>	
								<?php echo $foot_info['address_line_2']?>
							</p>
						</div>
						<div class="vertical vert-mobile"></div>
						<div class="foot-box-2">

							<a href="mailto:<?php echo $foot_info['email']?>"><p class="foot-email"><?php echo $foot_info['email']?></p></a>

							<a href="tel:<?php echo $foot_info['number']; ?>" class="phone-link track-phone-call-conversion"><p class="foot-num"><?php echo $foot_info['number']; ?></p></a>
						</div>
					</div>
				</div>
			</div>
			

			
			<div class="foot-social-banner">
				<p>Follow Us</p>
				<a href="<?php echo $foot_info['facebook']; ?>"><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/fb-logo-white.png"></a>
			</div>
		</div>
		
		<div id="foot-legal">
			<p id="foot-copy">
				Copyright <span>&#169;</span> <span id="foot-year">20xx</span> Cote Gelee. All rights reserved.
			</p>
			<p><a id="a1" href=<?php echo $foot_info['privacy_policy_link']['url']; ?>>Privacy Policy</a> | <a id="a2" href="https://gatorworks.net/">Site by Gatorworks</a></p>  
		</div>
		
	</div>
	
</footer>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	</div><!-- Close off-canvas content -->
<?php endif; ?>

<?php wp_footer(); ?>
<?php if (strpos(get_bloginfo('url'), "site")) { ?>
<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.3'><\/script>".replace("HOST", location.hostname));
//]]></script>
<?php } ?>
</body>
</html>
