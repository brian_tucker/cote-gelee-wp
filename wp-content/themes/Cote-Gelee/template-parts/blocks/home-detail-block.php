
<div class="home-detail-main-cont" id="parent-<?php the_ID(); ?>" class="parent-page">
	<div class="home-detail-header-cont">
			<h3>Featured Available Homes</h3>
			<a href="/homes">All Available Homes</a>
	</div>

	<div class="home-detail-innercont">
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array(
			    'post_type'      => 'homes',
			    'posts_per_page' => 3,
			    'paged'=> $paged,
			    'post_parent'    => $post->ID,
			    'order'          => 'ASC',
			    'orderby'        => 'menu_order'
			 );

			$homes = new WP_Query( $args );

		?>

	<?php
		$home_data = get_field('home_data');
		//THIS loops through the homes and inside the while loop is where you can
		//do all the logic to display the homes on the custom post type.
		//I WILL MAYBE TO also make it possible to add homes from the gutenberg view. These will be added at the end of the list.

		//if the post has any children, then it will
		if( $homes -> have_posts() ) :
		 while ( $homes->have_posts() ) : $homes->the_post(); ?>

		        		<?php

		        			$home_id = get_the_ID();
		        			$home_data = get_post_field('home_data', $homes->ID);

		        			$address = get_post_field('home_data_address_line_1', $home_id);
							$list_price = get_post_field('home_data_list_price', $home_id);
							$bedrooms = get_post_field('home_data_bedrooms', $home_id);
							$bathrooms = get_post_field('home_data_bathrooms', $home_id);
							$sq_footage = get_post_field('home_data_sq_footage', $home_id);
							$image_id = get_post_field('home_data_house_image', $home_id);
							$image = wp_get_attachment_url($image_id);

							$featured = get_post_field('home_data_featured', $home_id);
						?>

						<?php if($featured == "1"){ ?>

							<!-- this line are the children but clickable. Not sure if these should be clickable. -->
			            	<div class="house-detail-single-cont">
			            		<div class="house-detail-single-image-cont">
												<div class="house-detail-single-image-innercont">
														<div class="house-detail-single-image"><?php the_post_thumbnail(); ?></div>
												</div>


			            		</div>

			            		<div class="house-detail-single-text">
			            			<h3><?php echo $address?></h3>
			            			<hr id="home-hr1">
			            			<div class="house-detail-single-innertext">

										<div class="">
											<div class="house-detail-single-it-cont">
				            					<p class="home-text-title">List Price</p>
				            					<p class="home-text-value"><?php echo $list_price?></p>
				            				</div>

				            				<div class="house-detail-single-it-cont">
				            					<p class="home-text-title">Bedrooms</p>
				            					<p class="home-text-value"><?php echo $bedrooms?></p>
				            				</div>
										</div>

										<div class="">
				            				<div class="house-detail-single-it-cont">
				            					<p class="home-text-title">Bathrooms</p>
				            					<p class="home-text-value"><?php echo $bathrooms?></p>
				            				</div>

				            				<div class="house-detail-single-it-cont">
				            					<p class="home-text-title">Square Footage</p>
				            					<p class="home-text-value"><?php echo $sq_footage?></p>
				            				</div>
										</div>





			            			</div>
			            			<hr id="home-hr2">

			            			<div class="home-detail-button-cont">
			            				<div class="home-detail-button-innercont">
			            					<a class="home-detail-button" href="<?php the_permalink(); ?>">
			            						<p>View Details</p>
				            					<img src="/wp-content/themes/Cote-Gelee/dist/assets/images/b2.png">
				            				</a>
			            				</div>
			            			</div>
			            		</div>
			            	</div>
			            <?php }?> 
		    <?php endwhile; ?>
	</div>
	<a class="home-mobile-button" href="/homes">All Available Homes</a>
</div>
<?php endif; wp_reset_postdata(); ?>