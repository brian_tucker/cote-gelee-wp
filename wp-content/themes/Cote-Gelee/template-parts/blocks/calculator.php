<?php

$calc_info = get_field('calc_options', 'options'); 
$down_payment_options = $calc_info['down_payment'];
$mortgage_term_options = $calc_info['mortgage_term'];
?>

<div class="calc-main-cont">
	<hr>
	<hr>

	<h4 class="calc-main-title">Mortgage Estimator</h4>
	<form class="calc-input-cont">

		<div class="calc-input-innercont">
			<input id="home-value" class="calc-input" type="" name="" placeholder="Enter Home Value">
			<h4>Total Home Value</h4>	
		</div>

		<!-- down payment -->
		<div class="calc-input-innercont">
			<select id="down-payment" class="" type="" name="">
				<?php 
					foreach($down_payment_options as $option) { ?>
						<option><?php echo $option['value']?>%</option>
					<?php 
				}?>
			</select>
			<h4>Down Payment</h4>	
		</div>

		<div class="calc-input-innercont">
			<input id="interest-rate" class="calc-input" type="" name="" placeholder="Enter Interest Rate">
			<h4>Interest Rate</h4>	
		</div>

		<!-- mortgage term -->
		<div class="calc-input-innercont">
			<select id="mortgage-term" class="" type="" name="">
				<?php 
					foreach($mortgage_term_options as $m_options){ ?>
						<option><?php echo $m_options['value']?> years</option>
					<?php
					}
				?>
			</select>
			<h4>Mortgage Term</h4>	
		</div>
		<input id="calc-submit-button" type="button" onclick="calculateMortgageRate()" value="Calculate">
	</form>
	<div class="calc-result-cont">
		<div class="calc-result-innercont">
			<h4 id="invalid-data">Invalid Data Entered</h4>
			<h4 id="calc-result-head">Estimate:</h4>
			<p id="calc-result"></p>
		</div>
	</div>
	<div class="calc-note-cont">
		*This calculation is an estimate of loan principal, interest, property taxes and insurance. This feature is designed to provide a good faith estimate of a monthly payment based on a standard 30-year, fixed-rate mortgage. Buyers may be subject to additional fees associated with the loan based on their individual credit profile and loan structure.
	</div>
</div>

<script>
	function calculateMortgageRate() {

		//this is the flag to check to see if the data is valid or not. Default is false. 
		var passed = false;

		//total home value		
		var home_value = document.getElementById("home-value").value;

		//removing the dollar sign and commas if added
		var resultArr = handleNum(home_value);

		//splitting the results so they can be used
		resultArr = resultArr.split('|');

		//assigning the vars
		home_value = resultArr[0];
		passed = resultArr[1];


		//figure this out later....just reduce it from the home value.
		var down_payment = document.getElementById("down-payment").value; 

		//principal amount = p, subtracting the downpayment
		var principalAmount = 0;

		//checking to see if there is a downpayment or not.
		if(parseFloat(down_payment) > 0){
			var tmp =  percentage(parseFloat(home_value), parseFloat(down_payment));

			//subtracting the downpayment from the original amount
			principalAmount = parseFloat(home_value) - parseFloat(tmp);
		}
		else{
			principalAmount = parseFloat(home_value);
		}

		//interest rate = r
		var interest_rate =  document.getElementById("interest-rate").value; 
		//dividing rate by 100
		interest_rate = (parseFloat(interest_rate)/100);
		//getting the monthly interest rate
		interest_rate = interest_rate / 12;

		//term, need this to find n
		var mortgage_term =  document.getElementById("mortgage-term").value; 
		mortgage_term = mortgage_term.split(' ');
		mortgage_term = mortgage_term[0];

		//n = mortgage_term * 12;
		//n
		var mortgage_term_months = parseFloat(mortgage_term) * 12;
		
		//Do the logic to calculate the mortgage??? what is that?
		var M = Math.round(monthlyPayment(principalAmount, mortgage_term_months, interest_rate));

		if(passed)
		{
			$('#calc-result').text("$"+M + " per month.");
			$('#calc-result').css('display', 'block');
			$('#calc-result-head').css('display', 'block');
			$('#invalid-data').css('display', 'none');
		}
		else{
			$('#calc-result').css('display', 'none');
			$('#calc-result-head').css('display', 'none');
			$('#invalid-data').css('display', 'block');
		}

		
		$('.calc-result-cont').css('display', 'flex');
	}


	//function that calculates monthly payments
	function monthlyPayment(p, n, i) {
	  return p * i * (Math.pow(1 + i, n)) / (Math.pow(1 + i, n) - 1);
	}

	//function that calcalates percentage of a number
	function percentage(num, per){
	  return (num/100)*per;
	}

	//function that checks to see if string is a number, if not it returns false. If in dollar format, it strips off dollar chars
	function handleNum(num)
	{
		var passed = false;
		//checking to see if the home value is a number or not.
		if(num.match(/^[0-9]+$/) != null ){
			//already a number
			passed = true;
			return num + "|" + passed;
		}
		else
		{
			//not a number
			passed = false;

			if(num.includes('$'))
			{
				num = num.replace('$', '');
			}

			if(num.includes(','))
			{
				num = num.replace(',', '');
			}

			//checking to see if new value is a number
			if(num.match(/^[0-9]+$/) != null)
			{
				passed = true;
				return num + "|" + passed;
			}
			return "Failed: Home Value NaN.";
		}
	}

</script>