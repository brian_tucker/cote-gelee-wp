<?php
	$cta_data = get_field('cta');

	$image = $cta_data['image']['url'];
	$header = $cta_data['header'];
	$para = $cta_data['paragraph'];
	$button_text = $cta_data['button_text'];
	$button_link = $cta_data['button_link'];



?>


<div class="cta-cont">
	<div class="wp-block-group__inner-container">
		<div id="cta-bg1"></div>
		<figure class="wp-block-image size-large">
			<img src="<?php echo $image?>">
		</figure>
		<div class="cta-overlay"></div>
		<div class="wp-block-group">
			<div class="wp-block-group__inner-container reduce-margin">
				<h3><?php echo $header?></h3>
				<p><?php echo $para ?></p>
				<div class="wp-block-button">
					<a class="wp-block-button__link" href="<?php echo $button_link?>"><?php echo $button_text?></a>
				</div>
			</div>
		</div>
	</div>
</div>



<style type="text/css">
	
	#cta-bg1{
		/*background: #e19;*/
		background-image: url(<?php echo $image; ?>);
		height: 100%;
		width: 100%;
		background-size: cover;
		background-position: 29%, center;
	}

</style>