<?php
	$questions = get_field('questions');

	$title = $questions['title'];
	$description = $questions['description'];
	$input_1 = $questions['input_1'];
	$input_2 = $questions['input_2'];
	$input_3 = $questions['input_3'];
	$input_4 = $questions['input_4'];
	$input_5 = $questions['input_5'];
?>

<div class="questions-main-cont">
	<div class="questions-main-inner-cont">
		<div class="questions-header-cont">
			<h3><?php echo $title ?></h3>
			<p><?php echo $description ?></p>
			<p></p>
		</div>

		<div class="questions-input-cont">

			<div class="input-cont">
				<div>
					<input type="" name="">
					<p class="input-title"><?php echo $input_1; ?></p>
				</div>
				<div>
					<input type="" name="">
					<p class="input-title"><?php echo $input_2; ?></p>
				</div>
			</div>

			<div class="input-cont">
				<div>
					<input type="" name="">
					<p class="input-title"><?php echo $input_3; ?></p>
				</div>
				<div>
					<input type="" name="">
					<p class="input-title"><?php echo $input_4; ?></p>
				</div>
			</div>

		</div>

		<div class="question-comment-cont">
			<input type="" name="">
			<p class="input-title"><?php echo $input_5; ?></p>
		</div>

		<div class="question-button-cont">
			<a href=""><p class="question-button">Submit</p></a>
		</div>
	</div>
	
</div>