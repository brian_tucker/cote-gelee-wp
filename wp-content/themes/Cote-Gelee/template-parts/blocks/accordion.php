<?php 
	$faqs = get_field('faqs');	
?>

<div class="accordion-main-cont">
	 <div>
	 	<h3>Frequently Asked Questions</h3>	 	
	 	<ul class="vertical menu accordion-menu why-accordion" data-accordion-menu>
		<hr>
			<?php foreach($faqs as $faq) { ?>
		 		<li>
		 			<a href=""> <?php echo $faq['question']; ?>
		 				<div class="circle-cont">
		 					<div class="circle"></div>
		 				</div>
		 			</a>
		 			<ul class="menu vertical nested">
		 				<li>
		 					<a href="#"><?php echo $faq['answer']?></a>
		 				</li>
		 			</ul>
		 		</li>

		 		<hr>
			<?php }?>
	 	</ul>
	 </div>
</div>