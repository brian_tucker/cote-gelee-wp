<div class="slider-cont">
	<?php
		$images = get_field('slider_gallery');

		if( have_rows('slider_gallery') ) :
			echo '<div class="slider-for">';
				while ( have_rows('slider_gallery') ) : the_row();

				$image = get_sub_field('image');
				?>

					<div><img src="<?php echo $image['url']; ?>"/></div>
				<?php
				endwhile;
			echo '</div>
					<div class="slider-nav">';

				while(have_rows('slider_gallery') ) : the_row();

				$image = get_sub_field('image');	

				?>
					<div><img src="<?php echo $image['url']; ?>"/></div>
				<?php 
				endwhile;
			echo '</div>';

			

		endif;
				
				
		
?>

	<script type="text/javascript">
		$(document).ready(function(){

			HandleGalleryNav();

			// $('.slider-for').slick({
			//   slidesToShow: 1,
			//   slidesToScroll: 1,
			//   fade: true,
			//   asNavFor: '.slider-nav'
			// });
			// $('.slider-nav').slick({
			//   slidesToShow: 8,
			//   slidesToScroll: 1,
			//   asNavFor: '.slider-for',
			//   arrows: false,
			//   dots: false,
			//   centerMode: true,
			//   focusOnSelect: true
			// });
		});


		function HandleGalleryNav(){
			//defining the browswer width
			var width = window.innerWidth;

			// console.log("WIDTH: " + width);

			//large size
			if(width > 1050){
				$('.slider-for').slick({
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  fade: true,
				  asNavFor: '.slider-nav'
				});
				$('.slider-nav').slick({
				  slidesToShow: 8,
				  slidesToScroll: 1,
				  asNavFor: '.slider-for',
				  arrows: false,
				  dots: false,
				  centerMode: true,
				  focusOnSelect: true
				});
			}
			else{
				$('.slider-for').slick({
				  slidesToShow: 1,
				  slidesToScroll: 1,
				  fade: true,
				  asNavFor: '.slider-nav'
				});
				$('.slider-nav').slick({
				  slidesToShow: 2,
				  slidesToScroll: 1,
				  asNavFor: '.slider-for',
				  arrows: false,
				  dots: false,
				  centerMode: true,
				  focusOnSelect: true
				});
			}
		}

	</script>
</div>