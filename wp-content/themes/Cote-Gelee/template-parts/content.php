<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<div class="inner-blog-cont">
	<p class="inner-blog-date" ><?php echo get_the_date("m.d.y"); ?></p>
	<div class="inner-blog-cat-cont">


		<?php
			$categories = get_the_category();

			$i = 0;
			$len = count($categories);
		?>

		<p>
			<?php
			foreach($categories as $cat){
				if($i == $len - 1){?>
					<!-- //last -->
					<a href="<?php echo get_category_link($cat->cat_name); ?>">	<?echo $cat->cat_name; ?> </a>
					<?php
				}
				else
				{ ?>
						<a href="<?php echo get_category_link($cat->cat_name); ?>">	<?echo $cat->cat_name; ?> </a> <?php echo ", "?>
				<?php
				}
				$i++;
			}
			?>
		</p>

	</div>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header>
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		?>
			<?php foundationpress_entry_meta(); ?>
		</header>
		<div class="thumb-cont">
			<div class="thumb-innercont">
				<?php
					if ( has_post_thumbnail() ) {
					    the_post_thumbnail();
					}
				?>
			</div>
			
		</div>
		<div class="entry-content">
			<!-- <?php the_content(); ?> -->
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<p><?php echo get_the_excerpt(); ?></p>
			<button><a href="<?php the_permalink(); ?>"> Read More</a></button>
			<!-- <?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?> -->
		</div>

		<footer>
			<?php
				wp_link_pages(
					array(
						'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
						'after'  => '</p></nav>',
					)
				);
			?>
			<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
		</footer>
	</article>
</div>
<hr class="blog-hr" style="">
