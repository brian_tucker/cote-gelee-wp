<!-- displaying the cards that I find -->
<?php
		//getting the content from the global custom field
?>

<!-- the stuff below is used to display the children pages -->
<?php
	global $post; 

	$home_id = get_the_ID();

	$address = get_post_field('home_data_address_line_1', $home_id);
	$list_price = get_post_field('home_data_list_price', $home_id);
	$bedrooms = get_post_field('home_data_bedrooms', $home_id);
	$bathrooms = get_post_field('home_data_bathrooms', $home_id);
	$sq_footage = get_post_field('home_data_sq_footage', $home_id);
	$image_id = get_post_field('home_data_house_image', $home_id);
	$image = wp_get_attachment_url($image_id);

	$featured_img = get_post_thumbnail_id($home_id);

?>

<!-- this line are the children but clickable. Not sure if these should be clickable. -->
<div class="house-detail-single-cont">
	<div class="house-detail-single-image-cont">
		<div class="house-detail-single-image-innercont">
			<div class="home-detail-sold-overlay"><p class="">SOLD<p></div>
			<div class="house-detail-single-image"><?php the_post_thumbnail(); ?></div>
		</div>
	</div>
	<div class="house-detail-single-text">
		<h3><?php echo $address?></h3>
		<hr id="home-hr1">
		<div class="house-detail-single-innertext">

			<div class="">
				<div class="house-detail-single-it-cont">
					<p class="home-text-title">List Price</p>
					<p class="home-text-value"><?php echo $list_price?></p>
				</div>
				<div class="house-detail-single-it-cont">
					<p class="home-text-title">Bedrooms</p>
					<p class="home-text-value"><?php echo $bedrooms?></p>
				</div>
			</div>

			<div class="">
				<div class="house-detail-single-it-cont">
					<p class="home-text-title">Bathrooms</p>
					<p class="home-text-value"><?php echo $bathrooms?></p>
				</div>

				<div class="house-detail-single-it-cont">
					<p class="home-text-title">Square Footage</p>
					<p class="home-text-value"><?php echo $sq_footage?></p>
				</div>
			</div>

		</div>
		<hr id="home-hr2">

		<div class="home-detail-button-cont">
			<div class="home-detail-button-innercont">
				<a class="home-detail-button" href="<?php the_permalink(); ?>">
					<p>View Details</p>
					<img src="/wp-content/themes/Cote-Gelee/dist/assets/images/b2.png">
				</a>
			</div>
		</div>

	</div>
</div>