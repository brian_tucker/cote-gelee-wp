<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="main-container main-homes-cust">
	<div class="main-grid1">
		<div class="homes-archive-text-cont cust-page-header">
			<h2 class="homes-header">Available Homes</h2>
			<hr id="hr1">
			<hr id="hr2">
			<div class="homes-archive-text-innercont cust-page-innerheader">
				<h3 class="homes-sub-header">Start Your Journey</h3>
				<p class="homes-para">We have several homes ready for immediate occupancy or in various stages of construction. Our current inventory of homes
									available are shown below and feel free to contact the listing agent to schedule a tour for more information. </p>
			</div>
		</div>
		<div class="home-detail-main-cont" id="parent-<?php the_ID(); ?>" class="parent-page" style="width: auto;">
			<div>
				<?php

					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'homes' ),
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						'post_parent'			 => 0,
						'paged' => get_query_var('paged') ? get_query_var('paged') : 1
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();

							//the vars needed to get the status of the home
							$home_id = get_the_ID();
							$status = get_post_field('home_data_home_radio', $home_id);

							//filtering put the sold homes
							if($status == "Sold"){
								//don't show
							}
							else{
								get_template_part( 'template-parts/content-home');
							}
						}
					} else {
						// no posts found
					}

					// Restore original Post Data
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>	<!-- </main> -->
	<!-- </div> -->
	<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php
			if ( function_exists( 'foundationpress_pagination' ) ) :
				foundationpress_pagination();
			elseif ( is_paged() ) :
			?>
				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php endif; ?>
</div>

<?php

	//getting the page id using the name, the archive doesn't have an id
	$page_id = get_page_by_title('Homes')->ID;
	
	//the home data
	$archive_home_data = get_field('archive_home_data', $page_id);

	$title = $archive_home_data['title'];
	$para = $archive_home_data['paragraph'];

	$button_text = $archive_home_data['button']['title'];
	$button_link = $archive_home_data['button']['url'];

	$show_cta_1 = $archive_home_data['show_cta_1'];

?>

<?php 

	if($show_cta_1 == "Hide"){
		//not showing the cta
	}
	else{
		?>


		<!-- CTA START-->
		<div class="cta-cont">
			<div class="wp-block-group__inner-container">
				<div id="cta-bg"></div>
				<figure class="wp-block-image size-large">
					<img src="/wp-content/themes/Cote-Gelee/dist/assets/images/house-frame.png">
				</figure>
				<div class="cta-overlay"></div>
				<div class="wp-block-group">
					<div class="wp-block-group__inner-container">
						
						<h3><?php echo $title; ?></h3>
						<p><?php echo $para; ?></p>
						<div class="wp-block-button">
							<a class="wp-block-button__link more-width" href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- CTA END-->

		<?php
	}

?>



<hr id="hr1" class="above-sold-hr">
<hr id="hr2" class="above-sold-hr">

<!-- Sold Homes START -->
<div class="sold-homes-cont" style="padding-left: 7%; padding-right: 7%;">
	<div>
		<h3>Recently Sold Homes</h3>
		<div class="home-detail-main-cont sold-homes-no-padding" id="parent-<?php the_ID(); ?>" class="parent-page">
			<div>
				<?php
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'homes' ),
						'posts_per_page'         => '-1',
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						'post_parent'			 => 0
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();

							//me testing the query, getting the post id
							$home_id = get_the_ID();
							$address = get_post_field('home_data_address', $home_id);
							$status = get_post_field('home_data_home_radio', $home_id);

							if($status == "Sold Recently"){
								?>

							<script type="text/javascript">
								
								$('.cta-cont').addClass('margin-bottom');
								$('.sold-homes-cont').css('display', 'block');
								$('.above-sold-hr').css('display', 'block');

							</script>

								<?php
								get_template_part( 'template-parts/content-home');
							}
							else{
								//don't available homes
							}
						}
					} else {
						// no posts found
					}

					// Restore original Post Data
					wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</div>
<!-- Sold Homes END -->

<div class="wp-block-group front-group">
	<div class="wp-block-group__inner-container">
		<div class="wp-block-group g1">
			<div class="wp-block-group__inner-container">
				<figure class="wp-block-image size-large">
					<img class="full-height" src="/wp-content/themes/Cote-Gelee/dist/assets/images/cta-bg.png">
				</figure>
				<div class="wp-block-group">
					<div class="wp-block-group__inner-container">
						<p class="front-head">
							Claim Your Land
						</p>

						<p class="front-desc">
							Check out our map to see which lots are available to buy your home on.
						</p>

						<p class="front-link">
							<a href="/lots">See Available Lots</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="wp-block-group g2">
			<div class="wp-block-group__inner-container">

				<div class="q-contact-cont q-cont-tan q-cont-padding" style="height: 100%;">
					<!-- getting the content from the homes page -->
					<?php
						$page = get_page_by_title('homes');
						$content = apply_filters('the_content', $page->post_content);
						echo $content;
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
