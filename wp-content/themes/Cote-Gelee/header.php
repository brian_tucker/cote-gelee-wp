<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
$header_info = get_field('header_info', 'options'); 

$phone_number = $header_info['phone_number'];

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<header class="site-header" role="banner">
		<img id="nav-brush-stroke" src="/wp-content/themes/Cote-Gelee/dist/assets/images/nav-brush-stroke.png">
		<img id="mobile-nav-brush-stroke" src="/wp-content/themes/Cote-Gelee/dist/assets/images/mobile-nav-brush-stroke.png">


		<!-- This is the mobile menu -->
		<div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
			<div class="title-bar-left">
				<!-- <a href=""><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/phone.png"></a> -->
				<?php if ($phone_number != "") { ?>
					<!-- <a href="tel:<?php echo $phone_number; ?>" class="phone-link track-phone-call-conversion"><p class="fas fa-phone"><?php echo $phone_number?></p></a> -->
					<a href="tel:<?php echo $phone_number; ?>"><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/phone.png"></a>
				<?php } ?>
			</div>


			<div class="title-bar-center">
				<span class="site-mobile-title title-bar-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="/wp-content/themes/Cote-Gelee/dist/assets/images/mobile-logo.png"></a>
				</span>
			</div>

			<div class="title-bar-right">
				<!-- <button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button> -->

				<!-- This is the menu toggler -->
				<div class="toggle-container" data-toggle="offCanvas menu-toggler menu-text <?php foundationpress_mobile_menu_id(); ?>">
					<div id="menu-toggler" data-toggler=".open">
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					</div>
					<div id="menu-text" data-toggler=".open">
						<span class="menu-letters">menu</span>
						<span class="close-letters">close</span>
					</div>
				</div>
			</div>
		</div>

		<div class="site-navigation-overlay"></div>


		<!-- This is the full screen menu -->
		<nav class="site-navigation top-bar" role="navigation" id="<?php foundationpress_mobile_menu_id(); ?>">
			<div class="top-bar-left">
				<?php if ($phone_number != "") { ?>
						<a href="tel:<?php echo $phone_number; ?>" class="phone-link track-phone-call-conversion"><p class="fas fa-phone"><?php echo $phone_number?></p></a>
					<?php } ?>
				<div class="top-bar-left-menu">
					<?php foundationpress_top_bar_l(); ?>
				</div>

			</div>
			<div class="top-bar-center">
				<div class="site-desktop-title top-bar-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img id="main-logo" src="/wp-content/themes/Cote-Gelee/dist/assets/images/main-logo.png"></a>
				</div>
			</div>
			<div class="top-bar-right">
				<a id="blog-header" href="/blog"><p>BLOG</p></a>
				<div class="top-bar-right-menu">
					<?php foundationpress_top_bar_r(); ?>
				</div>


				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>

		<div class="off-canvas-wrapper">
			<div class="off-canvas position-right" id="offCanvas" data-off-canvas>
				<?php foundationpress_mobile_nav(); ?>
			</div>
		</div>

	</header>
